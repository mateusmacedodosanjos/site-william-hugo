<?php session_start(); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
    <link rel="stylesheet" href="style/css/style.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.css">

    <title>Proteção Máxima</title>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-gradient-dark">

    <div class="container">
        <a class="navbar-brand h1 mb-0" href="/index.php">Proteção Máxima</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSite">

            <ul class="navbar-nav ml-auto">

                <li class="navbar-item ml-5">
                    <a class="nav-link" href="/index.php">Página Inicial</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown ml-0">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropInstitucional">
                        Institucional
                    </a>
                    <div class="dropdown-menu dropdown bg-gradient">
                        <a class="dropdown-item" href="/quemsomos.php">Quem Somos</a>
                        <a class="dropdown-item" href="/ondeestamos.php">Onde Estamos</a>
                        <a class="dropdown-item" href="/nossosclientes.php">Nossos Clientes</a>
                        <a class="dropdown-item" href="/faleconosco.php">Fale Conosco</a>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown ml-0">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropServicos">
                        Serviços
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/escoltaarmada.php">Escolta Armada</a>
                        <a class="dropdown-item" href="/vigilanciapatrimonial.php">Vigilância Patrimonial</a>
                        <a class="dropdown-item" href="/segurancagrandeseventos.php">Segurança de Grandes Eventos</a>
                        <a class="dropdown-item" href="/segurancapessoalprivada.php">Segurança Pessoal Privada</a>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">

                <li class="navbar-item ml-4">
                    <a class="nav-link" href="/faleconosco.php">Contato</a>
                </li>
                <li class="navbar-item ml-4">
                    <a class="nav-link" href="/trabalheconosco.php">Trabalhe Conosco</a>
                </li>
                <li class="navbar-item ml-4">
                    <a class="nav-link" href="/acessocolaboradores.php">Acesso Colaboradores</a>
                </li>

            </ul>

        </div>
    </div>
</nav>

<div id="carouselSite" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">

        <div class="carousel-item active">
            <img src="imgs/banner-fixo.png" class="img-fluid d-block">

            <div class="carousel-caption d-none d-md-block text-dark">
                <h1 class="mb-1">BEM VINDO À PROTEÇÃO MÁXIMA</h1>
                <p class="lead">Para nós, a confiança e satisfação de nossos clientes é o mais importante.</p>
            </div>
        </div>

    </div>

</div>

<div class="container mt-3">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item "><a class="text-secondary" href="index.php">Página Inicial</a></li>
            <li class="breadcrumb-item "><a class="text-secondary" href="institucional.php">Institucional</a></li>
            <li class="breadcrumb-item active " aria-current="page">Fale Conosco
            <li>
        </ol>
    </nav>

    <div class="row justify-content-center">

        <div class="col-sm-12">
            <?php if (isset($_SESSION['mensagem'])) { ?>
                <div class="alert alert-dark" role="alert">
                    <?php echo($_SESSION['mensagem']); ?>
                </div>
            <?php unset($_SESSION['mensagem']); } ?>
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title display-4">Fale Conosco</h6>
                    <div class="col-12">
                        <div class="row mb-3">

                            <div class="col-sm-12">
                                <p class="lead">Saiba mais sobre nossos serviços, solicite orçamento ou esclareça
                                    dúvidas sobre a Proteção
                                    Máxima.</p>
                                <form method="post" action="sendmail_faleconosco.php">
                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label for="inputNome" class="lead">Nome</label>
                                            <input type="text" class="form-control" name="inputNome"
                                                   placeholder="Nome*" required="">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label for="inputEmail" class="lead">Email</label>
                                            <input type="text" class="form-control" name="inputEmail"
                                                   placeholder="Email*" required="">
                                        </div>
                                        <div class="col form-group">
                                            <label for="inputTelefone" class="lead">Telefone</label>
                                            <input type="text" class="form-control phone" name="inputTelefone"
                                                   placeholder="Telefone*" required="">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label for="inputCidade" class="lead">Cidade</label>
                                            <input type="text" class="form-control" name="inputCidade"
                                                   placeholder="Cidade*" required="">
                                        </div>
                                        <div class="col form-group">
                                            <label for="inputEstado" class="lead">Estado</label>
                                            <select name="inputEstado" class="form-control" required="">
                                                <option selected>Selecione o Estado</option>
                                                <option value="AC">Acre</option>
                                                <option value="AL">Alagoas</option>
                                                <option value="AP">Amapá</option>
                                                <option value="AM">Amazonas</option>
                                                <option value="BA">Bahia</option>
                                                <option value="CE">Ceará</option>
                                                <option value="DF">Distrito Federal</option>
                                                <option value="ES">Espírito Santo</option>
                                                <option value="GO">Goiás</option>
                                                <option value="MA">Maranhão</option>
                                                <option value="MT">Mato Grosso</option>
                                                <option value="MS">Mato Grosso do Sul</option>
                                                <option value="MG">Minas Gerais</option>
                                                <option value="PA">Pará</option>
                                                <option value="PB">Paraíba</option>
                                                <option value="PR">Paraná</option>
                                                <option value="PE">Pernambuco</option>
                                                <option value="PI">Piauí</option>
                                                <option value="RJ">Rio de Janeiro</option>
                                                <option value="RN">Rio Grande do Norte</option>
                                                <option value="RS">Rio Grande do Sul</option>
                                                <option value="RO">Rondônia</option>
                                                <option value="RR">Roraima</option>
                                                <option value="SC">Santa Catarina</option>
                                                <option value="SP">São Paulo</option>
                                                <option value="SE">Sergipe</option>
                                                <option value="TO">Tocantins</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label for="inputNomeEmpresa" class="lead">Nome da Empresa</label>
                                            <input type="text" class="form-control" name="inputEmpresa"
                                                   placeholder="Nome*" required="">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label for="inputServico" class="lead">Serviço de
                                                Interesse/Assunto</label>
                                            <select name="inputServico" class="form-control" required="">
                                                <option selected>Selecione o Serviço/Assunto</option>
                                                <option value="0">Vigilância Patrimonial
                                                </option>
                                                <option value="1">Escolta Armada</option>
                                                <option value="2">Segurança de Grandes Eventos
                                                </option>
                                                <option value="3">Segurança Pessoal Privada
                                                </option>
                                                <option value="4">Outro assunto</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col form-group">
                                            <label for="inputMensagem" class="lead">Mensagem</label>
                                            <textarea type="text" class="form-control" name="inputMensagem" rows="5"
                                                      placeholder="Mensagem*" required="" maxlength="500"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col form-group">
                                            <button type="submit" class="btn btn-lg btn-dark"
                                                    class="lead">Enviar
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>


                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-4">

            <h3 class="my-4 text-center">Institucional</h3>

            <div class="list-group list-group-flush text-center">
                <a href="/quemsomos.php" class="list-group-item list-group-item-action">Quem Somos</a>
                <a href="/ondeestamos.php" class="list-group-item list-group-item-action">Onde Estamos</a>
                <a href="/nossosclientes.php" class="list-group-item list-group-item-action">Nossos Clientes</a>
                <a href="/faleconosco.php" class="list-group-item list-group-item-action">Fale Conosco</a>
            </div>

        </div>

        <div class="col-sm-4">

            <h3 class="my-4 text-center">Menu</h3>

            <div class="list-group list-group-flush text-center">
                <a href="/index.php" class="list-group-item list-group-item-action">Início</a>
                <a href="/servicos.php" class="list-group-item list-group-item-action">Serviços</a>
                <a href="/trabalheconosco.php" class="list-group-item list-group-item-action">Trabalhe Conosco</a>
                <a href="/acessocolaboradores.php" class="list-group-item list-group-item-action">Acesso
                    Colaboradores</a>
            </div>

        </div>

        <div class="col-sm-4">
            <h3 class="my-4 text-center">Social</h3>
            <div class="list-group list-group-flush text-center">
                <a class="list-group-item list-group-item-action"
                   href="https://www.facebook.com/ProtecaoMaximaVigilanciaSeguranca" target="_blank"><i
                            class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a>
                <a class="list-group-item list-group-item-action" href="https://www.instagram.com/protecaomaxima/"
                   target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
                    Instagran</a>
                <p class="list-group-outline-item my-2 lead">Fique conectado conosco</p>
                <p class="list-group-outline-item my-2 lead">#ProteçãoMáxima</p>
            </div>
        </div>

    </div>

</div>

<div class="container-brand mt-3">
    <div class="row">
        <footer class="col-12">
            <p class="text-center">
                <span class="lead">PROTEÇÃO MÁXIMA VIGILÂNCIA E SEGURANÇA</span>
                <br>
                © Todos os direitos reservados.
                <br>
                <span>Rua Pio XII, 2144, Bairro São João Bosco</span>
                <br>
                <span>Porto Velho</span>
            </p>
        </footer>
    </div>
</div>

</div>
<!-- JavaScript (Opcional) -->
<!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
<script src="node_modules/jquery/dist/jquery.js"></script>
<script src="node_modules/popper.js/dist/umd/popper.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="js/jquery.inputmask.min.js"></script>
<script>
    // Exemplo de JavaScript inicial para desativar envios de formulário, se houver campos inválidos.
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Pega todos os formulários que nós queremos aplicar estilos de validação Bootstrap personalizados.
            var forms = document.getElementsByClassName('needs-validation');
            // Faz um loop neles e evita o envio
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    $(document).ready(function(){
        $('.phone').inputmask('(99)99999-9999');
    });
</script>

</div>

</body>

</html>