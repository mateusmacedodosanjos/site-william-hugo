<!DOCTYPE html>
<html lang="pt-br">

<head>
  <!-- Meta tags Obrigatórias -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
  <link rel="stylesheet" href="style/css/style.css">
  <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.css">

  <title>Proteção Máxima</title>
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-gradient-dark">

    <div class="container">
      <a class="navbar-brand h1 mb-0" href="/index.php">Proteção Máxima</a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSite">

        <ul class="navbar-nav ml-auto">

          <li class="navbar-item ml-5">
            <a class="nav-link" href="/index.php">Página Inicial</a>
          </li>
        </ul>

        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown ml-0">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropInstitucional">
              Institucional
            </a>
            <div class="dropdown-menu dropdown bg-gradient">
              <a class="dropdown-item" href="/quemsomos.php">Quem Somos</a>
              <a class="dropdown-item" href="/ondeestamos.php">Onde Estamos</a>
              <a class="dropdown-item" href="/nossosclientes.php">Nossos Clientes</a>
              <a class="dropdown-item" href="/faleconosco.php">Fale Conosco</a>
            </div>
          </li>
        </ul>

        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown ml-0">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropServicos">
              Serviços
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="/escoltaarmada.php">Escolta Armada</a>
              <a class="dropdown-item" href="/vigilanciapatrimonial.php">Vigilância Patrimonial</a>
              <a class="dropdown-item" href="/segurancagrandeseventos.php">Segurança de Grandes Eventos</a>
              <a class="dropdown-item" href="/segurancapessoalprivada.php">Segurança Pessoal Privada</a>
            </div>
          </li>
        </ul>

        <ul class="navbar-nav ml-auto">

            <li class="navbar-item ml-4">
                <a class="nav-link" href="/faleconosco.php">Contato</a>
            </li>
            <li class="navbar-item ml-4">
                <a class="nav-link" href="trabalheconosco.php">Trabalhe Conosco</a>
            </li>
            <li class="navbar-item ml-4">
                <a class="nav-link" href="/acessocolaboradores.php">Acesso Colaboradores</a>
            </li>
            
        </ul>

      </div>
    </div>
  </nav>

  <div id="carouselSite" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
      <li data-target="#carouselSite" data-slide-to="0" class="active">
      <li data-target="#carouselSite" data-slide-to="1">
      <li data-target="#carouselSite" data-slide-to="1">

      </li>
    </ol>

    <div class="carousel-inner">

      <div class="carousel-item active">
        <img src="imgs/banner1.jpg" class="img-fluid d-block">

        <div class="carousel-caption d-none d-md-block text-dark">
          <h1>TÍTULO BANNER 1</h1>
          <p class="lead">Descrição banner 1</p>
        </div>

      </div>

      <div class="carousel-item">
        <img src="imgs/banner2.jpg" class="img-fluid d-block">

        <div class="carousel-caption d-none d-md-block text-dark">
          <h1>TÍTULO BANNER 2</h1>
          <p class="lead">Descrição banner 2</p>
        </div>

      </div>

      <div class="carousel-item">
        <img src="imgs/banner3.jpg" class="img-fluid d-block">

        <div class="carousel-caption d-none d-md-block text-dark">
          <h1>TÍTULO BANNER 3</h1>
          <p class="lead">Descrição banner 3</p>
        </div>

      </div>

    </div>

    <a class="carousel-control-prev" href="#carouselSite" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
      <span class="sr-only">Anterior</span>
    </a>

    <a class="carousel-control-next" href="#carouselSite" role="button" data-slide="next">
      <span class="carousel-control-next-icon"></span>
      <span class="sr-only">Próximo</span>
    </a>

  </div>

  <div class="container">

    <div class="row my-3">

      <div class="col-12 text-center">
        <h1 class="display-4">Conheça nossos serviços</h1>
      </div>

    </div>

    <div class="row mb-12">
      <div class="col-sm-6 col-md-3">
        <div class="card mb-2">
          <img class="card-img-top" src="imgs/img-card.png">
          <div class="card-body text-center">
            <h6 class="card-title">Escolta Armada</h6>
            <p class="card-text mb-2 text-muted">Pequena descrição do serviço</p>
            <a href="/escoltaarmada.php" class="card-link">Saiba mais</a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-3">
        <div class="card mb-2">
          <img class="card-img-top" src="imgs/img-card.png">
          <div class="card-body text-center">
            <h6 class="card-title">Vigilância Patrimonial</h6>
            <p class="card-text mb-2 text-muted">Pequena descrição do serviço</p>
            <a href="/vigilanciapatrimonial.php" class="card-link">Saiba mais</a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-3">
        <div class="card mb-2">
          <img class="card-img-top" src="imgs/img-card.png">
          <div class="card-body text-center">
            <h6 class="card-title">Segurança de Grandes Eventos</h6>
            <p class="card-text mb-2 text-muted">Pequena descrição do serviço</p>
            <a href="/segurancagrandeseventos.php" class="card-link">Saiba mais</a>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-md-3">
        <div class="card mb-2">
          <img class="card-img-top" src="imgs/img-card.png">
          <div class="card-body text-center">
            <h6 class="card-title">Segurança Pessoal Privada</h6>
            <p class="card-text mb-2 text-muted">Pequena descrição do serviço</p>
            <a href="/segurancapessoalprivada.php" class="card-link">Saiba mais</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-sm-4">

        <h3 class="my-4 text-center">Institucional</h3>

        <div class="list-group list-group-flush text-center">
          <a href="/quemsomos.php" class="list-group-item list-group-item-action">Quem Somos</a>
          <a href="/ondeestamos.php" class="list-group-item list-group-item-action">Onde Estamos</a>
          <a href="/nossosclientes.php" class="list-group-item list-group-item-action">Nossos Clientes</a>
          <a href="/faleconosco.php" class="list-group-item list-group-item-action">Fale Conosco</a>
        </div>

      </div>

      <div class="col-sm-4">

        <h3 class="my-4 text-center">Menu</h3>

        <div class="list-group list-group-flush text-center">
          <a href="/index.php" class="list-group-item list-group-item-action">Início</a>
          <a href="/servicos.php" class="list-group-item list-group-item-action">Serviços</a>
          <a href="/trabalheconosco.php" class="list-group-item list-group-item-action">Trabalhe Conosco</a>
          <a href="/acessocolaboradores.php" class="list-group-item list-group-item-action">Acesso Colaboradores</a>
        </div>

      </div>

      <div class="col-sm-4">
        <h3 class="my-4 text-center">Social</h3>
        <div class="list-group list-group-flush text-center">
          <a class="list-group-item list-group-item-action" href="https://www.facebook.com/ProtecaoMaximaVigilanciaSeguranca" target="_blank"><i class="fa fa-facebook-official"
              aria-hidden="true"></i> Facebook</a>
          <a class="list-group-item list-group-item-action" href="https://www.instagram.com/protecaomaxima/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
            Instagran</a>
          <p class="list-group-outline-item my-2 lead">Fique conectado conosco</p>
          <p class="list-group-outline-item my-2 lead">#ProteçãoMáxima</p>
        </div>
      </div>
      
    </div> 

  </div>

  <div class="container-brand mt-3">
      <div class="row">
              <footer class="col-12">
                <p class="text-center">
                  <span class="lead">PROTEÇÃO MÁXIMA VIGILÂNCIA E SEGURANÇA</span>
                  <br>
                  © Todos os direitos reservados.
                  <br>
                  <span>Rua Pio XII, 2144, Bairro São João Bosco</span>
                  <br>
                  <span>Porto Velho</span>
                </p>
              </footer>
            </div>
          </div>
        </div>
  </div>


  </div>
  <!-- JavaScript (Opcional) -->
  <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
  <script src="node_modules/jquery/dist/jquery.js"></script>
  <script src="node_modules/popper.js/dist/umd/popper.js"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
</body>

</html>