<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
    <link rel="stylesheet" href="style/css/style.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.css">

    <title>Proteção Máxima</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-gradient-dark">

        <div class="container">
            <a class="navbar-brand h1 mb-0" href="/index.php">Proteção Máxima</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSite">

                <ul class="navbar-nav ml-auto">

                    <li class="navbar-item ml-5">
                      <a class="nav-link" href="/index.php">Página Inicial</a>
                    </li>
                  </ul>
          
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown ml-0">
                      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropInstitucional">
                        Institucional
                      </a>
                      <div class="dropdown-menu dropdown bg-gradient">
                        <a class="dropdown-item" href="/quemsomos.php">Quem Somos</a>
                        <a class="dropdown-item" href="/ondeestamos.php">Onde Estamos</a>
                        <a class="dropdown-item" href="/nossosclientes.php">Nossos Clientes</a>
                        <a class="dropdown-item" href="/faleconosco.php">Fale Conosco</a>
                      </div>
                    </li>
                  </ul>
          
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown ml-0">
                      <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropServicos">
                        Serviços
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="/escoltaarmada.php">Escolta Armada</a>
                        <a class="dropdown-item" href="/vigilanciapatrimonial.php">Vigilância Patrimonial</a>
                        <a class="dropdown-item" href="/segurancagrandeseventos.php">Segurança de Grandes Eventos</a>
                        <a class="dropdown-item" href="/segurancapessoalprivada.php">Segurança Pessoal Privada</a>
                      </div>
                    </li>
                  </ul>
          
                  <ul class="navbar-nav ml-auto">
          
                      <li class="navbar-item ml-4">
                          <a class="nav-link" href="/faleconosco.php">Contato</a>
                      </li>
                      <li class="navbar-item ml-4">
                          <a class="nav-link" href="/trabalheconosco.php">Trabalhe Conosco</a>
                      </li>
                      <li class="navbar-item ml-4">
                          <a class="nav-link" href="/acessocolaboradores.php">Acesso Colaboradores</a>
                      </li>
                      
                  </ul>

            </div>
        </div>
    </nav>

    <div id="carouselSite" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner">

            <div class="carousel-item active">
                <img src="imgs/banner-fixo.png" class="img-fluid d-block">

                <div class="carousel-caption d-none d-md-block text-dark">
                    <h1 class="mb-1">BEM VINDO À PROTEÇÃO MÁXIMA</h1>
                    <p class="lead">Para nós, a confiança e satisfação de nossos clientes é o mais importante.</p>
                </div>
            </div>

        </div>

    </div>

    <div class="container mt-3">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item "><a class="text-secondary" href="index.php">Página Inicial</a></li>
                <li class="breadcrumb-item "><a class="text-secondary" href="institucional.php">Institucional</a></li>
                <li class="breadcrumb-item active " aria-current="page">Nossos Clientes
                <li>
            </ol>
        </nav>

        <div class="row">

            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title display-4">Clientes</h5>
                        <div class="col-12">
                            <div class="row text-justify text-center lead my-4">

                                <p>Conheça nossos clientes:</p>

                                <div class="row mb-12">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-1">
                                            <img class="card-img-block ml-1" src="imgs/abi.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">ABI</h6>
                                                <p class="card-text mb-2 text-muted">Administração de Bens e
                                                    Infraestrutura</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-block" src="imgs/amaggi.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">AMAGGI</h6>
                                                <p class="card-text mb-2 text-muted">Exportação e Importação LTDA</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top m" src="imgs/agevisa.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">AGEVISA</h6>
                                                <p class="card-text mb-2 text-muted">Agência Estadual de Vigilância
                                                    emSaúde</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-block ml-1" src="imgs/cbm-ro.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">CBM-RO</h6>
                                                <p class="card-text mb-2 text-muted">Corpo de Bomeiros Militar de
                                                    Rondônia</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-12">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-block" src="imgs/detran.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">DETRAN</h6>
                                                <p class="card-text mb-2 text-muted">Departamento Estadual de Trânsito
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-block ml-1" src="imgs/petrobras.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">PETROBRÁS</h6>
                                                <p class="card-text mb-2 text-muted">Petrobrás Distribuidora S/A</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/rodobens.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">RODOBENS</h6>
                                                <p class="card-text mb-2 text-muted">Veículos e Comércio LTDA</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/seas.jpg">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">SEAS</h6>
                                                <p class="card-text mb-2 text-muted">Secretaria Estadual de Assistência
                                                    Social</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-12">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/sefin.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">SEFIN</h6>
                                                <p class="card-text mb-2 text-muted">Secretaria de Estado e Finanças</p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/sesau.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">SESAU</h6>
                                                <p class="card-text mb-2 text-muted">Secretaria de Estado e Saúde</p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/sesdec.jpg">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">SESDEC</h6>
                                                <p class="card-text mb-2 text-muted">Secretária de Estado Segurança,
                                                    Defesa e Cidadania</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/bertolini.png">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">BERTOLINI</h6>
                                                <p class="card-text mb-2 text-muted">Transportes Bertolini LTDA</p><br>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row mb-12">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/prf.png">
                                            <div class="card-body text-center justify">
                                                <h6 class="card-title">PRF</h6>
                                                <p class="card-text mb-2 text-muted">21ª Superintendência da Polícia
                                                    Rodoviárederal</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="card mb-2">
                                            <img class="card-img-top" src="imgs/white-solder.jpg">
                                            <div class="card-body text-center">
                                                <h6 class="card-title">WHITE SOLDER</h6>
                                                <p class="card-text mb-2 text-muted">Metalúrgica e Mineração LTDA</p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="mb-2">
                                            <img class="card-img-top m" src="imgs/white.jpg">
                                            <img class="card-img-top">
                                            <div class="card-body text-center">
                                                <h6 class="card-title"></h6>
                                                <p class="card-text mb-2 text-muted"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="mb-2">
                                            <img class="card-img-top m" src="imgs/white.jpg">
                                            <img class="card-img-top">
                                            <div class="card-body text-center">
                                                <h6 class="card-title"></h6>
                                                <p class="card-text mb-2 text-muted"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-sm-4">

                <h3 class="my-4 text-center">Institucional</h3>

                <div class="list-group list-group-flush text-center">
                    <a href="/quemsomos.php" class="list-group-item list-group-item-action">Quem Somos</a>
                    <a href="/ondeestamos.php" class="list-group-item list-group-item-action">Onde Estamos</a>
                    <a href="/nossosclientes.php" class="list-group-item list-group-item-action">Nossos Clientes</a>
                    <a href="/faleconosco.php" class="list-group-item list-group-item-action">Fale Conosco</a>
                </div>

            </div>

            <div class="col-sm-4">

                <h3 class="my-4 text-center">Menu</h3>

                <div class="list-group list-group-flush text-center">
                    <a href="/index.php" class="list-group-item list-group-item-action">Início</a>
                    <a href="/servicos.php" class="list-group-item list-group-item-action">Serviços</a>
                    <a href="/trabalheconosco.php" class="list-group-item list-group-item-action">Trabalhe Conosco</a>
                    <a href="/acessocolaboradores.php" class="list-group-item list-group-item-action">Acesso
                        Colaboradores</a>
                </div>

            </div>

            <div class="col-sm-4">
                <h3 class="my-4 text-center">Social</h3>
                <div class="list-group list-group-flush text-center">
                    <a class="list-group-item list-group-item-action"
                        href="https://www.facebook.com/ProtecaoMaximaVigilanciaSeguranca" target="_blank"><i
                            class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a>
                    <a class="list-group-item list-group-item-action" href="https://www.instagram.com/protecaomaxima/"
                        target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
                        Instagran</a>
                    <p class="list-group-outline-item my-2 lead">Fique conectado conosco</p>
                    <p class="list-group-outline-item my-2 lead">#ProteçãoMáxima</p>
                </div>
            </div>

        </div>

    </div>

    <div class="container-brand mt-3">
        <div class="row">
            <footer class="col-12">
                <p class="text-center">
                    <span class="lead">PROTEÇÃO MÁXIMA VIGILÂNCIA E SEGURANÇA</span>
                    <br>
                    © Todos os direitos reservados.
                    <br>
                    <span>Rua Pio XII, 2144, Bairro São João Bosco</span>
                    <br>
                    <span>Porto Velho</span>
                </p>
            </footer>
        </div>
    </div>

    </div>


    </div>
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="node_modules/jquery/dist/jquery.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
</body>

</html>