<?php

use Dotenv\Dotenv;

require_once('../vendor/autoload.php');
session_start();

$dotenv = Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();
$googleUrl = 'https://maps.googleapis.com/maps/api/js?key=' . getenv('GOOGLE_API_KEY') . '&callback=initMap';
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
    <link rel="stylesheet" href="style/css/style.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.css">

    <!-- jQuery library (served from Google) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <style>
        #map {
            height: 600px;
            width: 100%;
        }
    </style>

    <title>Proteção Máxima</title>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-gradient-dark">

    <div class="container">
        <a class="navbar-brand h1 mb-0" href="/index.php">Proteção Máxima</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSite">

            <ul class="navbar-nav ml-auto">

                <li class="navbar-item ml-5">
                    <a class="nav-link" href="/index.php">Página Inicial</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown ml-0">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropInstitucional">
                        Institucional
                    </a>
                    <div class="dropdown-menu dropdown bg-gradient">
                        <a class="dropdown-item" href="/quemsomos.php">Quem Somos</a>
                        <a class="dropdown-item" href="/ondeestamos.php">Onde Estamos</a>
                        <a class="dropdown-item" href="/nossosclientes.php">Nossos Clientes</a>
                        <a class="dropdown-item" href="/faleconosco.php">Fale Conosco</a>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown ml-0">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropServicos">
                        Serviços
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/escoltaarmada.php">Escolta Armada</a>
                        <a class="dropdown-item" href="/vigilanciapatrimonial.php">Vigilância Patrimonial</a>
                        <a class="dropdown-item" href="/segurancagrandeseventos.php">Segurança de Grandes Eventos</a>
                        <a class="dropdown-item" href="/segurancapessoalprivada.php">Segurança Pessoal Privada</a>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">

                <li class="navbar-item ml-4">
                    <a class="nav-link" href="/faleconosco.php">Contato</a>
                </li>
                <li class="navbar-item ml-4">
                    <a class="nav-link" href="/trabalheconosco.php">Trabalhe Conosco</a>
                </li>
                <li class="navbar-item ml-4">
                    <a class="nav-link" href="/acessocolaboradores.php">Acesso Colaboradores</a>
                </li>

            </ul>
        </div>
    </div>
</nav>

<div id="carouselSite" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner">

        <div class="carousel-item active">
            <img src="imgs/banner-fixo.png" class="img-fluid d-block">

            <div class="carousel-caption d-none d-md-block text-dark">
                <h1 class="mb-1">BEM VINDO À PROTEÇÃO MÁXIMA</h1>
                <p class="lead">Para nós, a confiança e satisfação de nossos clientes é o mais importante.</p>
            </div>
        </div>

    </div>

</div>

<div class="container mt-3">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item "><a class="text-secondary" href="index.php">Página Inicial</a></li>
            <li class="breadcrumb-item "><a class="text-secondary" href="institucional.php">Institucional</a></li>
            <li class="breadcrumb-item active " aria-current="page">Onde Estamos
            <li>
        </ol>
    </nav>

    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title display-4">Onde Estamos</h5>
                    <div class="col-12">
                        <div class="row text-justify lead my-4">
                            <p>
                                Conheça os locais onde estamos prestando serviços:
                            </p>
                            <div id="map" class="my-4">

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-4">

            <h3 class="my-4 text-center">Institucional</h3>

            <div class="list-group list-group-flush text-center">
                <a href="/quemsomos.php" class="list-group-item list-group-item-action">Quem Somos</a>
                <a href="/ondeestamos.php" class="list-group-item list-group-item-action">Onde Estamos</a>
                <a href="/nossosclientes.php" class="list-group-item list-group-item-action">Nossos Clientes</a>
                <a href="/faleconosco.php" class="list-group-item list-group-item-action">Fale Conosco</a>
            </div>

        </div>

        <div class="col-sm-4">

            <h3 class="my-4 text-center">Menu</h3>

            <div class="list-group list-group-flush text-center">
                <a href="/index.php" class="list-group-item list-group-item-action">Início</a>
                <a href="/servicos.php" class="list-group-item list-group-item-action">Serviços</a>
                <a href="/trabalheconosco.php" class="list-group-item list-group-item-action">Trabalhe Conosco</a>
                <a href="/acessocolaboradores.php" class="list-group-item list-group-item-action">Acesso
                    Colaboradores</a>
            </div>

        </div>

        <div class="col-sm-4">
            <h3 class="my-4 text-center">Social</h3>
            <div class="list-group list-group-flush text-center">
                <a class="list-group-item list-group-item-action"
                   href="https://www.facebook.com/ProtecaoMaximaVigilanciaSeguranca" target="_blank"><i
                            class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a>
                <a class="list-group-item list-group-item-action" href="https://www.instagram.com/protecaomaxima/"
                   target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
                    Instagran</a>
                <p class="list-group-outline-item my-2 lead">Fique conectado conosco</p>
                <p class="list-group-outline-item my-2 lead">#ProteçãoMáxima</p>
            </div>
        </div>

    </div>

</div>

<div class="container-brand mt-3">
    <div class="row">
        <footer class="col-12">
            <p class="text-center">
                <span class="lead">PROTEÇÃO MÁXIMA VIGILÂNCIA E SEGURANÇA</span>
                <br>
                © Todos os direitos reservados.
                <br>
                <span>Rua Pio XII, 2144, Bairro São João Bosco</span>
                <br>
                <span>Porto Velho</span>
            </p>
        </footer>
    </div>
</div>

</div>


</div>
<!-- JavaScript (Opcional) -->
<!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
<script src="node_modules/jquery/dist/jquery.js"></script>
<script src="node_modules/popper.js/dist/umd/popper.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script>
    function initMap() {
        //Opções do mapa
        var options = {
            zoom: 7,
            center: {lat: -10.8877, lng: -61.9474}
        }

        //Novo mapa
        var map = new google.maps.Map(document.getElementById('map'), options);

        addMarker({lat: -8.7612, lng: -63.9004});
        addMarker({lat: -9.74531, lng: -63.2834});
        addMarker({lat: -9.91375, lng: -63.044});
        addMarker({lat: -10.2124, lng: -63.8295});
        addMarker({lat: -10.341, lng: -62.8863});
        addMarker({lat: -10.5496, lng: -63.6175});
        addMarker({lat: -12.4155, lng: -64.2215});
        addMarker({lat: -9.7711, lng: -66.356});
        addMarker({lat: -10.7909, lng: -65.3322});
        addMarker({lat: -10.2516, lng: -63.2872});
        addMarker({lat: -9.70537, lng: -62.9005});
        addMarker({lat: -12.076025, lng: -64.027662});
        addMarker({lat: -12.0405, lng: -63.5721});
        addMarker({lat: -11.6934, lng: -62.6875});
        addMarker({lat: -11.7484, lng: -63.0306});

        //Função adicionar marcações
        function addMarker(coords) {
            var marker = new google.maps.Marker({
                position: coords,
                map: map
            });
        }
    }

</script>
<script async defer src="<?php echo $googleUrl; ?>"></script>
</body>
</html>