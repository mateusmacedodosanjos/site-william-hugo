<?php

use Dotenv\Dotenv;

require_once('../vendor/autoload.php');
session_start();

$dotenv = Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();

$nome = strip_tags(trim($_POST["inputNome"]));
$email = strip_tags(trim($_POST['inputEmail']));
$telefone = strip_tags(trim($_POST['inputTelefone']));
$cidade = strip_tags(trim($_POST['inputCidade']));
$estado = strip_tags(trim($_POST['inputEstado']));
$empresa = strip_tags(trim($_POST['inputEmpresa']));
$mensagem = strip_tags(trim($_POST['inputMensagem']));

if ($_POST['inputServico'] == 0) {
    $servico = "Vigilância Patrimonial";
} elseif ($_POST['inputServico'] == 1) {
    $servico = "Escolta Amrada";
} elseif ($_POST['inputServico'] == 2) {
    $servico = "Segurança de Grandes Eventos";
} elseif ($_POST['inputServico'] == 3) {
    $servico = "Seguranca Pessoal Privada";
} else {
    $servico = "Outros";
}

try {
    $message = new Swift_Message();
    $message->setSubject("Mensagem do Site");
    $message->setFrom(getenv('MAIL_FROM_MSG'), getenv('MAIL_FROM_ALIAS'));
    $message->addTo(getenv('MAIL_TO_MSG'));
    $body = "
        Nome: {$nome} <br> 
        Email: {$email} <br> 
        Telefone: {$telefone} <br> 
        Cidade: {$cidade} <br> 
        Estado: {$estado} <br> 
        Empresa: {$empresa} <br> 
        Serviço: {$servico} <br> 
        Mensagem: {$mensagem}
    ";
    $message->setBody($body, 'text/html');
    $transport = (new Swift_SmtpTransport(getenv('MAIL_HOST'), getenv('MAIL_PORT'), getenv('MAIL_ENCRYPTION')))
        ->setUsername(getenv('MAIL_USER'))
        ->setPassword(getenv('MAIL_PASS'));

    $mailer = new Swift_Mailer($transport);
    $result = $mailer->send($message);
    if ($result) {
        $_SESSION['mensagem'] = 'Sua mensagem foi enviada com sucesso!';
        header('Location: ' . '/faleconosco.php');
    }
} catch (Exception $exception) {
    $_SESSION['mensagem'] = $exception->getMessage();
    header('Location: ' . '/faleconosco.php');
}


