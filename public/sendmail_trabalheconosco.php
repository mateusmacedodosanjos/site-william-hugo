<?php

use Dotenv\Dotenv;

require_once('../vendor/autoload.php');
session_start();

$dotenv = Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();

function tirarAcentos($string)
{
    return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), $string);
}

$nome = strip_tags(trim($_POST["inputNome"]));
$email = strip_tags(trim($_POST['inputEmail']));
$telefone = strip_tags(trim($_POST['inputTelefone']));
$cidade = strip_tags(trim($_POST['inputCidade']));
$estado = strip_tags(trim($_POST['inputEstado']));
$file_tmp_path = $_FILES['inputArquivo']['tmp_name'];
$file_extension = pathinfo($_FILES['inputArquivo']['name'], PATHINFO_EXTENSION);
$file_name = 'Curriculo ' . tirarAcentos($nome) . '.' . $file_extension;

try {
    $attachment = Swift_Attachment::fromPath($file_tmp_path);
    $attachment->setFilename($file_name);
    $body = "
        Nome: {$nome} <br> 
        Email: {$email} <br> 
        Telefone: {$telefone} <br> 
        Cidade: {$cidade} <br> 
        Estado: {$estado} <br> 
        Empresa: {$empresa} <br> 
        Mensagem: {$mensagem}
    ";

    $message = new Swift_Message();
    $message->setSubject("Mensagem do Site");
    $message->setFrom(getenv('MAIL_FROM_CURRICULO'), getenv('MAIL_FROM_ALIAS'));
    $message->addTo(getenv('MAIL_TO_CURRICULO'), 'Currículos Proteção Máxima');
    $message->setBody($body, 'text/html');
    $message->attach($attachment);

    $transport = (new Swift_SmtpTransport(getenv('MAIL_HOST'), getenv('MAIL_PORT'), getenv('MAIL_ENCRYPTION')))
        ->setUsername(getenv('MAIL_USER'))
        ->setPassword(getenv('MAIL_PASS'));

    $mailer = new Swift_Mailer($transport);
    $result = $mailer->send($message);

    if ($result) {
        $_SESSION['mensagem'] = 'Sua mensagem foi enviada com sucesso!';
        header('Location: ' . '/trabalheconosco.php');
    }
} catch (Exception $exception) {
    $_SESSION['mensagem'] = $exception->getMessage();
    header('Location: ' . '/trabalheconosco.php');
}
